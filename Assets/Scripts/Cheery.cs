﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cheery : MonoBehaviour
{
    public string name = "None";
    public GameObject reviewRequest;
    public UIController uicontroller;

    void Init() {

    }
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player") {
          uicontroller.increaseScore();

          if (name == "Last") {
            reviewRequest.SetActive(true);
          }

          Destroy(gameObject);
        }
    }
}
