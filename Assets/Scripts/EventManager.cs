using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TentuPlay.Api; // import TentuPlay Api to gather/upload data
using System;
using System.IO;
using UnityEngine.UI;

public class EventManager : MonoBehaviour
{
    public string review_comments = "";
    public int rating = 0;
    public GameObject requestReview;
    public GameObject shop;
    public GameObject home;
    public GameObject inGameUI;
    public UIController uiController;

    public GameObject store_review;
    public GameObject purchase_complete;

    private string item = "";

    private string player_uuid = "믿보리"; // player_uuid can be anything that uniquely identifies each of your game user.
    private string character_uuid = TentuPlayKeyword._DUMMY_CHARACTER_ID_;
    private string[] character_uuids = new string[] { TentuPlayKeyword._DUMMY_CHARACTER_ID_ };

    public string thisStage = "STAGE";
    public string thisStageCategory = "test_MAIN_STORY";
    public string thisStageLevel = "test";
    private float player_currency = 0;
    private Dictionary<string, float> player_inventory = new Dictionary<string, float>();
    private System.Diagnostics.Stopwatch timer;

    TPStashEvent myStashEvent;

    // Start is called before the first frame update
    void Start()
    {
      myStashEvent = new TPStashEvent();
    }

    // Update is called once per frame
    void Update()
    {
      if (item == "Jump Explosion" && review_comments != "") {
        string star = "";
        for (int i = 0; i < rating; i++) {
          star += "★";
        }
        store_review.GetComponent<Text>().text = star + " " + review_comments;
      }
    }

    public void review_text_changed(string text) {
      review_comments = text;
    }

    public void rating1_changed(bool toggle) {
      if (toggle == true) {
        rating = 1;
        Debug.Log("1점");
      }
    }

    public void rating2_changed(bool toggle) {
      if (toggle == true) {
        rating = 2;
        Debug.Log("2점");
      }
    }

    public void rating3_changed(bool toggle) {
      if (toggle == true) {
        rating = 3;
        Debug.Log("3점");
      }
    }

    public void rating4_changed(bool toggle) {
      if (toggle == true) {
        rating = 4;
        Debug.Log("4점");
      }
    }

    public void rating5_changed(bool toggle) {
      if (toggle == true) {
        rating = 5;
        Debug.Log("5점");
      }
    }

    public void click_send_review() {
      Debug.Log(string.Format("review: {0}, rating: {1}", review_comments, rating));
      Debug.Log("Send!!");

      // 파일 저장
      string file_name = "UserData.txt";
      string time = System.DateTime.Now.ToString("yyyyMMdd");

      int level = uiController.getLevel();
      string row = player_uuid + "//" + item + "//" + "1" + "//" + string.Format("{0}", level) + "//" + string.Format("{0}", rating) + "//" + review_comments + "//" + time;
      string path = Application.dataPath + "/" + file_name;
      if (File.Exists(Application.dataPath + "/" + file_name)) {
        using (StreamWriter sww = new StreamWriter(new FileStream(path, FileMode.Append))) {
          sww.WriteLine(row);
        }
      }
      else {
        using (StreamWriter sww = new StreamWriter(new FileStream(path, FileMode.Create))) {
          string header = "user id" + "//" + "item" + "//" + "item use count" + "//" + "Level" + "//" + "rating" + "//" + "comments" + "//" + time;
          sww.WriteLine(header);
          sww.WriteLine(row);
        }
      }
      // 파일 저장
      myStashEvent.PlayStageWithPet(
        player_uuid: player_uuid,
        character_uuid: review_comments,
        item_slug: item,
        stage_slug: string.Format("{0}", rating),
        stage_type: stageType.PvE,
        stage_category_slug: string.Format("{0}", level)
      );

      /*
      myStashEvent.InAppPurchase(
            player_uuid: player_uuid, // unique identifier of player
            character_uuid: review_comments,
            purchasable_slug: item,// unique identifier of bought item
            purchase_quantity: rating,
            purchase_unit_price: level,
            purchase_total_price: 0.0F,
            purchase_currency_code: currencyCode.USD
            );
            */
      new TPUploadData().UploadData(true);


      requestReview.SetActive(false);
      goHome();
    }

    public void click_purchase_item_one() {
      Configuration.speed = 12f;
      Configuration.JumpSpeed = Configuration.getOriginJumpSpeed();

      item = "Speed Up";
      Debug.Log("Speed Up");
      myStashEvent.InAppPurchase(
            player_uuid: player_uuid, // unique identifier of player
            character_uuid: character_uuid,
            purchasable_slug: item,// unique identifier of bought item
            purchase_quantity: 1.0F,
            purchase_unit_price: 0.0F,
            purchase_total_price: 0.0F,
            purchase_currency_code: currencyCode.USD
            );

      new TPUploadData().UploadData(true);

      purchase_complete.SetActive(true);
    }

    public void click_purchase_item_second() {
      Configuration.speed = Configuration.getOriginSpeed();
      Configuration.JumpSpeed = 9f;

      item = "Jump Up";
      Debug.Log("Jump Up");
      myStashEvent.InAppPurchase(
            player_uuid: player_uuid, // unique identifier of player
            character_uuid: character_uuid,
            purchasable_slug: item,// unique identifier of bought item
            purchase_quantity: 1.0F,
            purchase_unit_price: 0.0F,
            purchase_total_price: 0.0F,
            purchase_currency_code: currencyCode.USD
            );
      new TPUploadData().UploadData(true);

      purchase_complete.SetActive(true);
    }

    public void click_purchase_item_third() {
      Configuration.speed = Configuration.getOriginSpeed();
      Configuration.JumpSpeed = 10f;

      item = "Jump Explosion";
      Debug.Log("Jump Explosion");
      myStashEvent.InAppPurchase(
            player_uuid: player_uuid, // unique identifier of player
            character_uuid: character_uuid,
            purchasable_slug: item,// unique identifier of bought item
            purchase_quantity: 1.0F,
            purchase_unit_price: 0.0F,
            purchase_total_price: 0.0F,
            purchase_currency_code: currencyCode.USD
            );
      new TPUploadData().UploadData(true);

      purchase_complete.SetActive(true);
    }

    public void gameStart() {
      purchase_complete.SetActive(false);

      home.SetActive(false);
      shop.SetActive(false);
      inGameUI.SetActive(true);

      Debug.Log("Game Start");

      // If you want anything to be seen on the dashboard console, Join method is neccessary.
      myStashEvent.Join(player_uuid: player_uuid);

      // Calling UploadData after Join method is recommended.
      new TPUploadData().UploadData(true);


      myStashEvent.LoginApp(player_uuid: player_uuid);


      new TPUploadData().UploadData(true);


      //also writes stage start
      myStashEvent.PlayStageWithEquipment(
          player_uuid: player_uuid, // unique identifier of player
          character_uuid: character_uuid,
          item_slug : item,
          stage_type: stageType.PvE,
          stage_slug: thisStage, // unique identifier of played stage
          stage_category_slug: thisStageCategory // category slug of stage (optional)
          );

      new TPUploadData().UploadData(true);

      timer = new System.Diagnostics.Stopwatch();
      timer.Start();
    }

    public void goShop() {
      home.SetActive(false);
      shop.SetActive(true);
      purchase_complete.SetActive(false);

      Debug.Log("Go Shop");
    }

    public void goHome() {
      shop.SetActive(false);
      home.SetActive(true);

      Debug.Log("Go Home");
    }

    public void goGame() {
      requestReview.SetActive(false);
      shop.SetActive(false);
      home.SetActive(false);
      inGameUI.SetActive(true);
    }
}
