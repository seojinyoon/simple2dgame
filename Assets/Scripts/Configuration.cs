using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Configuration : MonoBehaviour
{
    public static float speed = 8f;
    public static float JumpSpeed = 8f;

    public static float getOriginSpeed() {
      return 8f;
    }

    public static float getOriginJumpSpeed() {
      return 8f;
    }
}
