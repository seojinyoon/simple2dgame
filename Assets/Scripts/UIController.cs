using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public int score = 0;
    public GameObject scoreText;
    public int level = 0;

    // Start is called before the first frame update
    void Start()
    {
      scoreText.GetComponent<Text>().text = string.Format("{0}: {1}", "score", score);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void increaseScore() {
      score += 1;
      scoreText.GetComponent<Text>().text = string.Format("{0}: {1}", "score", score);
    }

    public int getLevel() {
      if (score >= 5) {
        level = 2;
      }
      else if (score < 5 && score >= 2) {
        level = 1;
      }
      else {
        level = 0;
      }

      return level;
    }
}
